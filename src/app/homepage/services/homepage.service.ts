import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Pub } from 'src/app/models/Pub.model';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' }) //Hacer que esté disponible en toda la app
export class HomePageService {

  apiUrl: string = 'https://jsonplaceholder.typicode.com/posts'; // url para peticiones
  apiData: object[] = [];

  constructor(private http: HttpClient) { }


  getData(): Observable<Pub[]> {  //metodo para obtener las publicaciones
    return this.http.get<Pub[]>(this.apiUrl);
    ;
  }

}
