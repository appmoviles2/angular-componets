import { Component, OnInit } from '@angular/core';
import { HomePageService } from '../../services/homepage.service';
import { Pub } from 'src/app/models/Pub.model';

@Component({
  selector: 'app-posts-page',
  templateUrl: './posts-page.component.html',
  styleUrls: ['./posts-page.component.scss']
})

export class PostsPageComponent implements OnInit {

  publicaciones: Pub[] = [];

  constructor(private homePageService: HomePageService){ }

  //Hacer la petición al cargar el componente
  ngOnInit(): void {
    this.getPostData();
  }

  getPostData(){
    this.homePageService.getData().subscribe(data => {
      this.publicaciones = data;
    });
  }

}
