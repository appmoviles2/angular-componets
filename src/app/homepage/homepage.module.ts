import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutPageComponent } from './pages/about-page/about-page.component';
import { ContactPageComponent } from './pages/contact-page/contact-page.component';
import { PostsPageComponent } from './pages/posts-page/posts-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';



@NgModule({
  declarations: [
    AboutPageComponent,
    ContactPageComponent,
    PostsPageComponent,
    HomePageComponent
  ],
  imports: [
    CommonModule
  ]
})
export class HomepageModule { }
