import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AboutPageComponent } from './homepage/pages/about-page/about-page.component';
import { ContactPageComponent } from './homepage/pages/contact-page/contact-page.component';
import { PostsPageComponent } from './homepage/pages/posts-page/posts-page.component';
import { HomePageComponent } from './homepage/pages/home-page/home-page.component';



//Rutas de la aplicación: tipado routes
const routes: Routes = [
  {
    //Ruta
    path: '',
    //Qué va a mostrar
    component: HomePageComponent,
  },
  {
    //Ruta
    path: 'about',
    //Qué va a mostrar
    component: AboutPageComponent,
  },
  {
    //Ruta
    path: 'contact',
    //Qué va a mostrar
    component: ContactPageComponent,
  },
  {
    //Ruta
    path: 'posts',
    //Qué va a mostrar
    component: PostsPageComponent,
  }
];


@NgModule({
  //Se indica que es el router principal
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
