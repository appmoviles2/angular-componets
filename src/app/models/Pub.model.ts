export interface Pub {  //interfaz  para las publicaciones
  id: number;       // id de publicacion
  title: string;    // tituilo de publicacion
  body: string;   // cuerpo de la publicacion
}
